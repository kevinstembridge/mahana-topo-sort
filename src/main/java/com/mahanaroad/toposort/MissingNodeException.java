package com.mahanaroad.toposort;

import java.util.HashSet;
import java.util.Set;

/**
 * Indicates that a topological sort was attempted on a node graph 
 * where 1 or more of the nodes depended upon by other nodes were
 * not listed as top-level nodes. E.G. NodeA depends on NodeB, but
 * NodeB was not added to the graph. 
 * 
 */
public class MissingNodeException extends RuntimeException {

    private static final long serialVersionUID = -6767363718248723885L;

    private final Set<String> missingNodeIdSet = new HashSet<>();


    private static String createMessage(Set<String> missingNodeIdSet) {
        return "The following nodes are listed as pre-requisites but they "
               + "have not been added as nodes: "
               + missingNodeIdSet;
    }
    
    
    public MissingNodeException(Set<String> nodeIds) {
        super(createMessage(nodeIds));
        
        if (nodeIds != null) {
            this.missingNodeIdSet.addAll(nodeIds);
        }
        
    }

    
    /**
     * @return Never null.
     */
    public final Set<String> getMissingNodeIdSet() {
        return new HashSet<>(this.missingNodeIdSet);
    }
    
    
}
