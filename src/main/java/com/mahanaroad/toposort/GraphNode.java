package com.mahanaroad.toposort;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;



/**
 * This class represents a node in a graph of dependencies between nodes.
 * It includes references, by name, to all the other nodes that it has
 * a direct dependency on.
 * 
 */
public final class GraphNode<ITEM> {

    private final String nodeName;
    private final Set<String> dependsOnNodes = new HashSet<>();
    private ITEM item;

    
    /**
     * @param nodeName Must not be null or blank. Must be unique within the full graph of nodes.
     * @param item The optional item to be stored in the node.
     * 
     * @throws IllegalArgumentException if {@code nodeName} is null or blank.
     */
    public GraphNode(String nodeName, ITEM item) {
        
        if (nodeName == null || "".equals(nodeName.trim())) {
            throw new IllegalArgumentException("nodeName must not be null or blank");
        }

        this.nodeName = nodeName;
        this.item = item;
        
    }
    
    
    public void addDependencies(String... nodeNames) {
        
        if (nodeNames != null) {
            Collections.addAll(this.dependsOnNodes, nodeNames);
        }
        
    }
    

    public void removeDependencyOnNode(GraphNode<?> graphNode) {
        
        Iterator<String> itr = this.dependsOnNodes.iterator();
        
        while (itr.hasNext()) {
            
            String nextTableName = itr.next();
            
            if (nextTableName.equals(graphNode.getNodeName())) {
                itr.remove();
            }
            
        }
                
    }

    
    public void removeDependencies(GraphNode<?>... graphNodes) {
        
        if (graphNodes != null) {
            
            for (GraphNode<?> graphNode : graphNodes) {
                removeDependencyOnNode(graphNode);
            }
            
        }
        
    }

    
    public final String getNodeName() {
        return this.nodeName;
    }

    
    @Override
    public String toString() {
        
        StringBuilder builder = new StringBuilder();
        builder.append("Node{");
        builder.append(this.nodeName);
        buildDependenciesString(builder);
        builder.append("}");
        return builder.toString();
        
    }
    
    
    private void buildDependenciesString(StringBuilder builder) {
        
        if (!this.dependsOnNodes.isEmpty()) {
            builder.append("->");
            
            
            Iterator<String> itr = this.dependsOnNodes.iterator();
            
            while (itr.hasNext()) {
                String nodeId = itr.next();
                builder.append(nodeId);
                
                if (itr.hasNext()) {
                    builder.append(", ");
                }
                
            }
            
        }
        
    }
    
    
    @Override
    public boolean equals(Object obj) {

        if (!(obj instanceof GraphNode)) {
            return false;
        }

        GraphNode<?> other = (GraphNode<?>) obj;
        return this.nodeName.equals(other.nodeName);

    }

    
    @Override
    public int hashCode() {

        int result = 17;
        result += 37 * this.nodeName.hashCode();
        return result;

    }


    public boolean hasNoDependencies() {
        return this.dependsOnNodes.isEmpty();
    }
    
    
    public Set<String> getDependsOnNodes() {
        return new HashSet<>(this.dependsOnNodes);
    }

    
    public ITEM getItem() {
        return this.item;
    }
    
}
